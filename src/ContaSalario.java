public class ContaSalario extends Conta {
        private int quantidadeSaque;

    public ContaSalario(int numero, int agencia, String banco, double saldo, double saque, int qtdSaque, double deposito){
        super(numero, agencia, banco, saldo, saque, deposito);
        this.quantidadeSaque = qtdSaque;

    }

    public int getQtdSaque() {
        return quantidadeSaque;
    }

    public void setQtdSaque(int qtdSaque) {
        this.quantidadeSaque = qtdSaque;
    }

    @Override
    public double getSaque() {
        return 0;
    }

    @Override
    public double getDeposito() {
        return 0;
    }


    @Override
    public double getSaldo() {
        return 0;
    }

    @Override
    public String toString() {
        return "ContaSalario{" +
                "qtdSaque=" + quantidadeSaque +
                '}';
    }

}




