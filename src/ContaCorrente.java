public class ContaCorrente extends Conta {

    private double chequeEspecial;

    public ContaCorrente(int numero, int agencia, String banco, double saldo, double saque, double deposito, double chequeEspecial) {
        super(numero, agencia, banco, saldo, saque, deposito);
        this.chequeEspecial = chequeEspecial;
    }

    @Override
    public String toString() {
        return "ContaCorrente{" +
                "chequeEspecial=" + chequeEspecial +
                '}';
    }

    public double getSaque() {
        return this.saldo-this.saque;
    }

    public double getDeposito() {
        return this.saldo+this.deposito;
    }

    public double getSaldo() {
        return this.chequeEspecial + this.saldo;
    }

}
