public class TestaConta {

    public static void main(String[] args){

        ContaCorrente cc1 = new ContaCorrente(22, 1, "Banco AA", 100.00,2000.00,2100.00,6000.00 );
        System.out.println("Extrato da Conta Corrente");
        System.out.println("O saldo da conta corrente é R$: " +cc1.getSaldo());
        System.out.println("O valor limite de saque diário é R$: " +cc1.saque);
        System.out.println("O valor do depósito é R$: " +cc1.deposito);

        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------");

        ContaPoupanca p1 = new ContaPoupanca(33, 3, "Banco CCC", 10.00, 20, 00.05, 20.00, 2000.00);
        System.out.println("Extrato da Conta Poupança");
        System.out.println("O saldo da conta Poupança é R$: " +p1.getSaldo());
        System.out.println("O valor do saque é R$: " +p1.saque);
        System.out.println("O valor limite de deposito diário é R$: " +p1.deposito);

        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------");

        ContaSalario cs1 = new ContaSalario(44,4,"Banco DDD",1631.00,1631.00,5,100);
        System.out.println("Extrato da Conta Salário");
        System.out.println("O saldo da Conta Salário é R$: " +cs1.saldo);
        System.out.println("A quantidade de saques mensais é de: " +cs1.getQtdSaque());



    }
}